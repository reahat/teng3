﻿#include "command\Script.h"
#include "command\Command.h"
#include "cocos2d.h"
#include "lib\clx\utf8\get.h"
#include "command\CommandListHeader.h"

#pragma execution_character_set("utf-8")

using namespace std;
USING_NS_CC;

/*
スクリプト仕様：
 + クリック待ち
 : キャラクター名の後に付ける
 | 明示的な改行
 & メッセージの改行を伴わないスクリプトの改行
 # コメント
 @ メッセージ以外のコマンドprefix
 [ ルビを伴う文字列の開始指定
 ] ルビを伴う文字列の終了指定
 { ルビの開始指定
 } ルビの終了指定
*/


Script::Script(TextBox* textBox, string fileName)
{
	this->textBox = textBox;
	FileUtils* fileUtil = FileUtils::getInstance();
	fileUtil->addSearchPath("scripts");
	fileUtil->setPopupNotify(true);
	list<string> scriptLine = splitString(fileUtil->getStringFromFile(fileName), "\r\n");
	convertScriptsToCommands(scriptLine);
}

Script::~Script()
{
	//cmdListをdeleteする
	//for (auto cmd : cmdList){
	//	delete cmd;
	//}
}

// 読み込んだテキストは、一行の巨大なstringになる。
// これを改行コードで分割する。
list<string> Script::splitString(const string& scriptString, const string& delimiter)
{
	list<string> res;
	size_t current = 0, found, delimlen = delimiter.size();
	while ((found = scriptString.find(delimiter, current)) != string::npos){
		res.push_back(string(scriptString, current, found - current));
		current = found + delimlen;
	}
	res.push_back(string(scriptString, current, scriptString.size() - current));
	return res;
}

// 実スクリプト行単位で分割されたstringを、複数行にわたるものは１つのlistとして
// それをコマンド化する。
// script_list: スクリプトの一行がstring一つに対応
void Script::convertScriptsToCommands(list<string>& script_list){
	list<string> fragments;
	// line: 実スクリプトの一行
	for (auto line : script_list){
		// スペースかカンマで分割
		list<string> tmp = splitString(line, " ");
		// 行末がコロン（名前）かパイプ（Msg/Say改行）のとき、次行も同じコマンドとする
		if (isContinuousLine(tmp.back())){
			// 結合
			fragments.splice(fragments.end(), tmp);
			continue;
		}
		fragments.splice(fragments.end(), tmp);

		cmdList.push_back(createCommand(fragments));
		fragments.clear();
	}
}

Command* Script::createCommand(list<string>& scriptFragments){
	// デバッグ用のテキストリストを生成
	string scriptLine;
	for (auto str : scriptFragments){
		scriptLine += str + " ";
	}
	scriptList.push_back(scriptLine);

	// コマンド作成
	list<string> args;
	string firstArg = "";
	for (auto str : scriptFragments){
		if (str != ""){
			args.push_back(str);
			if (firstArg == "") firstArg = str;
		}
	}

	
	if (args.size() == 0){
		// 空行
	}else if (firstArg.at(0) == '#'){
		// コメント行
	}
	else if(firstArg.at(0) != '@'){
		// メッセージまたはセリフ
		if (isCmdSay(firstArg)){
			// @say
		//	return static_cast<Command*>(new CmdMessage(args));
		}
		else{
			// @msg
			return static_cast<Command*>(new CmdMessage(textBox, args));
		}
	}
	else{
		// 通常コマンド

	}







}

// 文字列の「最後の文字」がコロン・パイプであるかどうかを判定
bool Script::isContinuousLine(string& str){
	string dest;
	string::iterator pos = str.begin();
	while (pos != str.end()){
		dest.clear();
		clx::utf8::get(pos, str.end(), insert_iterator<string>(dest, dest.end()));
	}
	if (dest == ":" || dest == "|") return true;
	return false;
}

// コマンドがSayかMsgかを判定
bool Script::isCmdSay(string& str){
	string dest;
	string::iterator pos = str.begin();
	while (pos != str.end()){
		dest.clear();
		clx::utf8::get(pos, str.end(), insert_iterator<string>(dest, dest.end()));
	}
	if (dest == ":") return true;
	return false;
}