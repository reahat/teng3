#ifndef __TENG3__CmdMessage__
#define __TENG3__CmdMessage__

#include "command\Command.h"
#include <string>
#include <list>
#include "scene\TextBox.h"
using namespace std;

class CmdMessage: public Command
{
public:
	CmdMessage(TextBox* textBox, list<string> args);
	~CmdMessage();
private:
	list<string> msgLine;
	bool isContainLineBreak(string str);
};

#endif /* defined(__TENG3__CmdMessage__) */