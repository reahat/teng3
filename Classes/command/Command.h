#ifndef __TENG3__Command__
#define __TENG3__Command__

#include <list>
#include <string>

using namespace std;

class Command
{
public:
	Command();
	virtual ~Command();
private:
	int tag;
	list<string> args;
		
	

};

#endif /* defined(__TENG3__Command__) */