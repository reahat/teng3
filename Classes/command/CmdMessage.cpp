#include "command\CmdMessage.h"
#include "lib\clx\utf8.h"

CmdMessage::CmdMessage(TextBox* textBox, list<string> args){
	const int MSG_MAX_CHARS = 60; // 半角60文字以内。全角は2文字として扱う
	string buff;
	for (string str : args){
		string::iterator pos = str.begin();
		int cnt = 0;

		// argsごとに１文字ずつ取り出しbuffに格納する
		while (pos != str.end()){
			string dest;
			clx::utf8::get(pos, str.end(), insert_iterator<string>(dest, dest.end()));
			// 半角文字の場合
			if (dest.size() == 1){
				cnt += 1;
			}
			// 全角文字の場合
			else{
				cnt += 2;
			}

			// 改行文字の場合、それをバッファに入れずに次の行へ
			if (dest == "|"){
				if (!buff.empty()){
					msgLine.push_back(buff);
				}
				buff.clear();
				cnt = 0;
			}
			// 通常は1文字バッファに追加
			else if (cnt <= MSG_MAX_CHARS){
				buff += dest;
			}
			// バッファが溢れた場合、次の行へ
			else{
				msgLine.push_back(buff);
				buff.clear();
				buff += dest;
				// 次の行の文字数をリセット
				cnt = dest.size() == 1 ? 1 : 2;
			}
		}
		if(!buff.empty())msgLine.push_back(buff);
		buff.clear();
	}

	// DEBUG
	//int i = 0;
	//for (string a : msgLine){
	//	i++;
	//	log("%d: %s", i, a.c_str());
	//}
}

CmdMessage::~CmdMessage(){

}

// 文字列の「最後の文字」がパイプであるかどうかを判定
bool CmdMessage::isContainLineBreak(string str){
	string dest;
	string::iterator pos = str.begin();
	while (pos != str.end()){
		dest.clear();
		clx::utf8::get(pos, str.end(), insert_iterator<string>(dest, dest.end()));
	}
	if (dest == "|") return true;
	return false;
}
