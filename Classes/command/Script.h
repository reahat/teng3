#ifndef __TENG3__Script__
#define __TENG3__Script__

#include <string>
#include <list>
#include "command\Command.h"
#include "scene\TextBox.h"

using namespace std;

class Script
{
public:
	Script(TextBox* textBox, std::string fileName);
	~Script();

	list<string> getScriptList(){ return scriptList; }
	int size(){ return scriptList.size(); }
private:
	list<string> splitString(const string& scriptString, const string& delimiter);
	void convertScriptsToCommands(list<string>& script_list);
	Command* createCommand(list<string>& scriptFragments);
	list<string> splitScriptLine(string& script_line);
	bool isContinuousLine(string& str);
	bool isCmdSay(string& str);

	list<string> scriptList;
	list<Command*> cmdList;
	TextBox* textBox;
};


#endif /* defined(__TENG3__Script__) */