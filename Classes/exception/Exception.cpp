#include "exception\Exception.h"
#include "cocos2d.h"
#include <Windows.h>

USING_NS_CC;

void Exception::raise(std::string errorCode, std::string errorMsg){
	std::string header = "An error occurred.\n\n";
	std::string code = "";
	if (errorCode != ""){
		code = "E-" + errorCode + ":\n";
	}
	std::string msg = header + code + errorMsg;

	// メッセージボックス表示
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	MessageBoxA(NULL, msg.c_str(), "Application error", MB_ICONERROR);
#endif

	// 終了処理
	Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void Exception::raise(std::string errorMsg){

}