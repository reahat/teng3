#ifndef __TENG3__Exception__
#define __TENG3__Exception__

#include <string>

// RAISE
#define RAISE(strErrorCode,strErrorMsg) Exception::raise(strErrorCode,strErrorMsg)

class Exception{
public:
	~Exception(){}
	static void raise(std::string errorCode, std::string errorMsg);
	static void raise(std::string errorMsg);
private:
	Exception(){}
};

#endif /* defined(__TENG3__Exception__) */