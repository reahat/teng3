#ifndef __TENG3__SceneDebug__
#define __TENG3__SceneDebug__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "lib\ScrollBarView\ScrollBarView.h"
//#include "ui\CocosGUI.h"
#include "scene\SceneScenario.h"


USING_NS_CC;
USING_NS_CC_EXT;

class SceneDebug
	: public SceneScenario
	, public TableViewDataSource
	, public TableViewDelegate
{
protected:
    SceneDebug();
    virtual ~SceneDebug();
	//virtual bool init();
	bool init(Script* script);
public:
	// TableViewDataSourceの抽象メソッド
	virtual Size cellSizeForTable(TableView* table);
	virtual TableViewCell* tableCellAtIndex(TableView* table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(TableView* table);

	// TableViewDelegateの抽象メソッド
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);

	// TableViewDelegateが継承しているScrollViewの抽象メソッド
	virtual void scrollViewDidScroll(ScrollView* view);
	virtual void scrollViewDidZoom(ScrollView* view){};

	ScrollBarView* scrollBarView;

    static cocos2d::Scene* createScene(Script* script);
    void update(float dt);

	// CREATE_FUNCの代わり
	static SceneDebug* create(Script* script);
    //CREATE_FUNC(SceneDebug);

	CC_SYNTHESIZE(Script*, _debugScript, DebugScript);

private:
	bool isInit;
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) override;

};

#endif /* defined(__TENG3__SceneDebug__) */