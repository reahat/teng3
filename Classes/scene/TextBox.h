#ifndef __TENG3__TextBox__
#define __TENG3__TextBox__

#include "cocos2d.h"
#include <string>
USING_NS_CC;
using namespace std;

class TextBox :public cocos2d::Layer
{
protected:
	TextBox();
	virtual ~TextBox();
	bool init() override;

public:
	static TextBox* createTextBox();

	void update(float dt);
	CREATE_FUNC(TextBox);

	CC_SYNTHESIZE(string, _text, Text);
	CC_SYNTHESIZE_RETAIN(Label*, _textLabel, TextLabel);
	CC_SYNTHESIZE_RETAIN(Sprite*, _imgBase, ImgBase);
	
private:

};

#endif /* defined(__TENG3__TextBox__) */