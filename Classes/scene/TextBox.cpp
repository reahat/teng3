#include "scene\TextBox.h"

TextBox::TextBox()
: _textLabel(NULL)
, _text("")
{
	// 画像検索基本パス
	FileUtils::getInstance()->addSearchPath("images/scenario/ui/textbox");
}

TextBox::~TextBox(){

}

TextBox* TextBox::createTextBox(){
	log("textbox create");
	auto layer = TextBox::create();
	auto tmpImgBase = Sprite::create("text_box_base.png");
	auto baseSize = tmpImgBase->getContentSize();
	layer->setContentSize(baseSize);
	return layer;
}

bool TextBox::init(){
	log("textbox init");
	if (!Layer::init()){
		return false;
	}
	_imgBase = Sprite::create("text_box_base.png");
	this->addChild(_imgBase);
	_imgBase->setPosition(Vec2(0, 0));

	return true;
}


void TextBox::update(float dt){

}