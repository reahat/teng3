#include "scene/SceneTitle.h"
#include "scene/SceneScenario.h"
#include "util/MouseOverMenuItem.h"

USING_NS_CC;
using namespace std;

const int BUTTON_X = 50;
const int BUTTON_TOP_Y = 400;
const int BUTTON_DISTANCE = 10;

SceneTitle::SceneTitle()
{
	// 画像検索基本パス
	FileUtils::getInstance()->addSearchPath("images/title");

}

SceneTitle::~SceneTitle(){
	//CC_SAFE_RELEASE_NULL(_mainLayer);
}

Scene* SceneTitle::createScene(){
	auto scene = Scene::create();
	auto layer = SceneTitle::create();
	scene->addChild(layer);
	return scene;
}

bool SceneTitle::init(){
	if (!Layer::init()){
		return false;
	}
	auto director = Director::getInstance();
	// 画面サイズ取得
	auto winSize = director->getWinSize();
	// メイン描画レイヤ
	_mainLayer = Layer::create();
	_mainLayer->setCascadeOpacityEnabled(true);
	this->addChild(_mainLayer);

	// 背景生成
	auto background = Sprite::create("background.jpg");
	// 画像のぼけを回避
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		background->getTexture()->setAliasTexParameters();
	#endif
	background->setPosition(Vec2(winSize.width / 2.0, winSize.height / 2.0));
	_mainLayer->addChild(background);

	createButtons();

	this->scheduleUpdate();

	return true;
}

void SceneTitle::update(float dt){
}

void SceneTitle::createButtons(){
	// ボタン生成
	auto btn_start = MouseOverMenuItem::create("start_0.png", "start_01.png", "start_02.png");
	auto btn_exit = MouseOverMenuItem::create("exit_0.png", "exit_01.png", "exit_02.png");
	btn_start->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	btn_exit->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	btn_start->setPosition(Vec2(BUTTON_X, BUTTON_TOP_Y));
	btn_exit->setPosition(Vec2(BUTTON_X, BUTTON_TOP_Y - 100));

	btn_start->setCursorInCallback([this](Ref* ref){
		log("cursor in START");
	});

	btn_start->setButtonPressedCallback([this](Ref* ref){});
	btn_start->setButtonReleasedCallback([this](Ref* ref){
		auto fadeOut = FadeOut::create(0.3);
		auto sequence = Sequence::create(fadeOut, CallFunc::create([](){
			Director::getInstance()->replaceScene(SceneScenario::createScene());
		}), NULL);
		_mainLayer->runAction(sequence);
	});

	btn_exit->setCursorInCallback([this](Ref* fer){
		log("cursor in EXIT");
	});
	btn_exit->setButtonPressedCallback([this](Ref* ref){});
	btn_exit->setButtonReleasedCallback([this](Ref* ref){
		gameExit();
	});

	_mainLayer->addChild(btn_start);
	_mainLayer->addChild(btn_exit);

}

void SceneTitle::gameExit(){
	auto fadeOut = FadeOut::create(0.5);
	auto sequence = Sequence::create(fadeOut, CallFunc::create([](){
		Director::getInstance()->end();
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			exit(0);
		#endif
	}), NULL);
	_mainLayer->runAction(sequence);
}