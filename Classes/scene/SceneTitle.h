#ifndef __TENG3__SceneTitle__
#define __TENG3__SceneTitle__

#include "cocos2d.h"

class SceneTitle :public cocos2d::Layer
{
protected:
	SceneTitle();
	virtual ~SceneTitle();
	bool init() override;

public:
	static cocos2d::Scene* createScene();

	void update(float dt);
	CREATE_FUNC(SceneTitle);
	
	CC_SYNTHESIZE_RETAIN(cocos2d::Layer*, _mainLayer, MainLayer);

	void createButtons();
	void gameExit();
private:

};

#endif /* defined(__TENG3__SceneTitle__) */