#include "scene/sceneScenario.h"
#include "scene/SceneDebug.h"
#include "util/MouseOverMenuItem.h"
#include "exception\Exception.h"

USING_NS_CC;
using namespace std;

SceneScenario::SceneScenario()
: _mainLayer(NULL)
, _textBox(NULL)
{

}

SceneScenario::~SceneScenario(){
	//CC_SAFE_RELEASE_NULL(_textBox);
	// TODO Scriptを開放する
}

Scene* SceneScenario::createScene(){
	auto scene = Scene::create();
	auto layer = SceneScenario::create();
	scene->addChild(layer);
	return scene;
}

bool SceneScenario::init(){
	if (!Layer::init()){
		return false;
	}
	// メイン描画レイヤ作成
	_mainLayer = Layer::create();
	_mainLayer->setCascadeOpacityEnabled(true);
	this->addChild(_mainLayer);

	// 入力リスナ
	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(SceneScenario::onKeyPressed, this);
	auto director = Director::getInstance();
	director->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	// 画像ロード
	createTextBox();

	// スクリプト読み込み
	_script = new Script(_textBox, "k001.asc");

	this->scheduleUpdate();

	return true;
}

void SceneScenario::update(float dt){

}


void SceneScenario::createTextBox(){
	auto director = Director::getInstance();
	auto winSize = director->getWinSize();

	//_textBox = TextBox::create();
	_textBox = TextBox::createTextBox();

	_mainLayer->addChild(_textBox);
	int textBoxLocationY = _textBox->getContentSize().height / 2 + 10;
	_textBox->setPosition(Vec2(winSize.width / 2, textBoxLocationY));
}

void SceneScenario::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event){
	switch (keyCode)
	{
	case cocos2d::EventKeyboard::KeyCode::KEY_ESCAPE:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_CTRL:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_F10:
		log("Push Debug scene.");
		Director::getInstance()->pushScene(SceneDebug::createScene(_script));
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_SPACE:
		break;
	default:
		break;
	}
}