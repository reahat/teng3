﻿#include "scene\SceneDebug.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

SceneDebug::SceneDebug()
: isInit(false)
{

}

SceneDebug::~SceneDebug(){

}

SceneDebug* SceneDebug::create(Script* script){
	auto p = new SceneDebug();
	if (p->init(script)) {
		p->autorelease();
		return p;
	}
	else {
		delete p;
		return nullptr;
	}
}

Scene* SceneDebug::createScene(Script* script){
	auto scene = Scene::create();
	auto layer = SceneDebug::create(script);
	scene->addChild(layer);
	return scene;
}


bool SceneDebug::init(Script* script){
	if (!Layer::init()){
		return false;
	}

	_debugScript = script;
	
	// モーダル処理
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = [](Touch* touch, Event* event)->bool{
		return true;
	};
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	dispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	auto winSize = Director::getInstance()->getWinSize();

	TableView* tableView = TableView::create(this, winSize * 0.8);
	// 展開方向
	tableView->setDirection(TableView::Direction::VERTICAL);
	// 表示を上から下へ
	tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	tableView->setBounceable(false);

	// 追加
	tableView->setDelegate(this);
	this->addChild(tableView);
	tableView->reloadData();


	scrollBarView = ScrollBarView::create(tableView, ScrollBarView::BarType::VERTICAL_OUT);

	// 入力リスナ
	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(SceneDebug::onKeyPressed, this);
	auto director = Director::getInstance();
	director->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	
	isInit = true;
	return true;
}

// セルの大きさを設定する
Size SceneDebug::cellSizeForTable(TableView *table){
	auto winSize = Director::getInstance()->getWinSize();
	return Size(winSize.width, 25);
}

// 1セルに表示させるValueをセット
TableViewCell* SceneDebug::tableCellAtIndex(TableView *table, ssize_t idx){
	auto winSize = Director::getInstance()->getWinSize();
	std::string id = StringUtils::format("%d", idx);
	//std::string text = StringUtils::format("Line %d", idx);
	auto scriptList = _debugScript->getScriptList();
	list<string>::iterator scriptIter = scriptList.begin();
	advance(scriptIter, idx);
	std::string text = *scriptIter;


	TableViewCell *cell = table->dequeueCell();




	if (cell && cell->getIdx() >= 0){
		// セルの再使用
		static_cast<LabelTTF*>(cell->getChildByTag(3))->setString(id.c_str());
		static_cast<LabelTTF*>(cell->getChildByTag(4))->setString(text.c_str());
	}else{
		cell = new TableViewCell();
		cell->autorelease();
		// セルの背景は交互に色を変更する
		auto background_color = Color3B(255, 255, 255);
		if (idx % 2) {
			background_color = Color3B(200, 200, 200);
		}

		Sprite* bg = Sprite::create();
		bg->setAnchorPoint(Point(0, 0));
		bg->setTextureRect(Rect(0, 0, winSize.width * 0.8, 24));
		bg->setColor(background_color);
		bg->setTag(100);
		cell->addChild(bg);

		// ボーダーライン
		Sprite* line = Sprite::create();
		line->setAnchorPoint(Point(0, 0));
		line->setTextureRect(Rect(0, 0, winSize.width * 0.8, 1));
		line->setColor(Color3B(0, 0, 0));
		cell->addChild(line);

		// ID部分
		auto *label_1 = LabelTTF::create(id.c_str(), "Arial", 20);
		label_1->setAnchorPoint(Point(0, 0));
		label_1->setPosition(Point(50, 0));
		label_1->setColor(Color3B(0, 0, 0));
		cell->addChild(label_1);

		// テキスト部分
		auto *label_2 = LabelTTF::create(text.c_str(), "Arial", 20);
		label_2->setAnchorPoint(Point(0, 0));
		label_2->setPosition(Point(100, 0));
		label_2->setColor(Color3B(0, 0, 0));
		cell->addChild(label_2);

	}

	return cell;
}

// セル数
ssize_t SceneDebug::numberOfCellsInTableView(TableView *table){
	return _debugScript->size();
}

// セルがタッチされた時のcallback
void SceneDebug::tableCellTouched(TableView* table, TableViewCell* cell){
	log("%iのセルがタッチされました", cell->getIdx());
}

void SceneDebug::scrollViewDidScroll(ScrollView* view){
	if (isInit){
		scrollBarView->refresh();
	}
}


void SceneDebug::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event){
	switch (keyCode)
	{
	case cocos2d::EventKeyboard::KeyCode::KEY_ESCAPE:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_CTRL:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_F10:
		log("Pop Debug scene.");
		Director::getInstance()->popScene();
		break;
	case cocos2d::EventKeyboard::KeyCode::KEY_SPACE:
		break;
	default:
		break;
	}
}







void SceneDebug::update(float dt){

}