#ifndef __TENG3__SceneScenario__
#define __TENG3__SceneScenario__

#include "cocos2d.h"
#include "command\Script.h"
#include "scene\TextBox.h"

class SceneScenario :public cocos2d::Layer
{
protected:
	SceneScenario();
	virtual ~SceneScenario();
	bool init() override;

public:
	static cocos2d::Scene* createScene();

	void update(float dt);
	CREATE_FUNC(SceneScenario);
	
	CC_SYNTHESIZE_RETAIN(cocos2d::Layer*, _mainLayer, MainLayer);
	CC_SYNTHESIZE_RETAIN(TextBox*, _textBox, TextBox);

	CC_SYNTHESIZE(Script*, _script, Script);
private:
	// キーボード入力時コールバック
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) override;
	
	void createTextBox();

};

#endif /* defined(__TENG3__SceneScenario__) */